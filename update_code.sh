echo updating code;
cd /home/gitlab/rubber-duck;
git fetch;
git checkout $1;
pip3 install -r requirements.txt;
pidof python3 | nohup python3 main.py &>/dev/null &
echo update done;
exit 1;
