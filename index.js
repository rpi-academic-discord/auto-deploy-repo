var express = require('express')
var bodyParser = require('body-parser')
var app = express()
var jsonParser = bodyParser.json()

app.get('/new_commit', function (req, res) {
  res.send("OK")
})

// const shell = require('shelljs');
var exec = require('child_process').exec, child;
app.post('/new_commit', jsonParser, function (req, res) {
  // create user in req.body
  console.log(req.body)
  res.send("OK")
  console.log(req.body.object_attributes.ref)
  console.log(req.body.object_attributes.detailed_status)
  if(req.body.object_attributes.ref == "master" && req.body.object_attributes.detailed_status == "passed"){
    // shell.exec('bash update_code.sh ' + req.body.object_attributes.sha,function(){
    //   console.log("The update is done, waiting for next update")
    // })
    console.log("starting exec")
    exec('bash update_code.sh ' + req.body.object_attributes.sha,
    function (error, stdout, stderr) {
        console.log('stdout: ' + stdout);
        console.log('stderr: ' + stderr);
        if (error !== null) {
             console.log('exec error: ' + error);
        }
        console.log("The update is done, waiting for next update")
    });
    console.log("moving on")
  }
})

app.get('/', (req, res) => {
  res
    .status(200)
    .send('Hello, world!')
    .end();
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
